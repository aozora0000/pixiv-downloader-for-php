<?php
class ApiTest extends PHPUnit_Framework_TestCase
{
    static $api = "http://www.kancolle-ero.info/api/sapi";
    static $noapi = "http://www.kancolle-ero.info/api/api";

    public function testGetIteratorSuccessCase() {
        $apiResult = Pixiv\Model\Api::getIterator(static::$api);
        $this->assertInstanceOf("ArrayIterator",$apiResult);
    }

    public function testGetIteratorFailCase() {
        try {
            $apiResult = Pixiv\Model\Api::getIterator(static::$noapi);
            $this->fail("Exception投げられてない！");
        } catch(\Pixiv\Exception\Exception $e) {
            $this->assertTrue(true);
        }

    }
}
