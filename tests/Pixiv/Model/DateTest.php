<?php
class DateTest extends PHPUnit_Framework_TestCase
{
    public function testDataToTimeCaseSuccess() {
        $date = "2014年10月04日 10:10";
        $datetime = Pixiv\Model\Date::dataToTime($date);
        $this->assertEquals("2014-10-04 10:10:00",$datetime);
    }
    public function testDateToTimeCaseFail() {
        $date = "aaa";
        try {
            $datetime = Pixiv\Model\Date::dataToTime($date);
            $this->fail("例外が投げられていません");
        } catch(\Pixiv\Exception\Exception $e) {
            $this->assertTrue(true);
        }
    }
}
