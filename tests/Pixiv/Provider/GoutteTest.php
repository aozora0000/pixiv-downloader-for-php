<?php
/**
 * @medium
 * @Group Provider
 * @coversDefaultClass \Pixiv\Provider\Goutte\Goutte
 */
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class GoutteTest extends PHPUnit_Framework_TestCase
{
    protected function setUp() {
        $logger = new Logger('goutte');
        $logger->pushHandler(new StreamHandler('php://stdout', Logger::WARNING));

        $this->client = new Pixiv\Provider\Goutte\Goutte($logger, "2014-10-10 00:00:00");
        $this->login_id = getenv("LOGIN_USER");
        $this->login_pass = getenv("LOGIN_PASS");
    }
    /**
     * @covers ::__construct
     */
    public function testConstruct() {
        $this->assertInstanceOf("Pixiv\Provider\Goutte\Goutte",$this->client);
    }

    /**
     * @covers ::login
     */
    public function testLoginCaseSuccess() {
        $result = $this->client->login($this->login_id,$this->login_pass);
        $this->assertTrue($result,"ログインに失敗しています。");
    }

    /**
     * @covers ::createSearchUrl
     */
    public function testCreateSearchUrl() {
        Closure::bind(function(){
            $obj = new Pixiv\Provider\Goutte\Goutte((new Logger('goutte'))->pushHandler(new StreamHandler('/dev/null')));
            $url = $obj->createSearchUrl("test",true);
            $this->assertEquals("http://www.pixiv.net/search.php?s_mode=s_tag&r18=1&word=test",$url);
        }, $this, 'Pixiv\Provider\Goutte\Goutte')->__invoke();
    }

    /**
     * @covers ::search
     * @covers ::is_next
     */
    public function testSearch() {
        $this->testLoginCaseSuccess();
        $urls = $this->client->search("test",true);
        $this->assertInstanceOf("ArrayObject",$urls);
    }

    /**
     * @covers ::getImages
     */
    public function testGetImages() {
        $this->testLoginCaseSuccess();
        $urls = $this->client->search("test",true);
        $this->client->getImages($urls,"/tmp/test/");

        $finder = new Symfony\Component\Finder\Finder();
        $iterator = $finder->in("/tmp/test/")->files();
        $this->assertGreaterThanOrEqual(count($urls),iterator_to_array($iterator));
        $fs = new Symfony\Component\Filesystem\Filesystem;
        $fs->remove('/tmp/test/*');
    }
}
