<?php
namespace Pixiv\Provider\Goutte;
use \Goutte\Client;
use \Pixiv\Model\Date;
use \GuzzleHttp\Subscriber\Retry\RetrySubscriber;

class Goutte extends \Pixiv\Core\Provider implements \Pixiv\Interfaces\ProviderInterface {

    public function __construct($logger,$date_limit = "1970-01-01 09:01:00") {
        $this->client = new Client();
        $this->logger = $logger;
        $this->client->getClient()->setDefaultOption('config/curl/'.CURLOPT_TIMEOUT, 60);
        $this->client->getClient()->getEmitter()->attach(new RetrySubscriber([
            'filter' => RetrySubscriber::createStatusFilter()
        ]));
        $this->client->setHeader('User-Agent','Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:35.0) Gecko/20100101 Firefox/35.0');
        $this->date_limit = $date_limit;
        $this->is_login = false;
    }

    public function login($login_id, $login_pass) {
        if($this->is_login) {
            return true;
        }
        $crawler = $this->client->request('GET', static::$login_url);
        $form = $crawler->selectButton("ログイン")->form();
        $login_params = array(
            "pixiv_id"=>$login_id,
            "pass"=>$login_pass
        );
        $crawler = $this->client->submit($form, $login_params);
        try {
            $class = $crawler->filter("body")->attr("class");
            if($class === "not-logged-in") {
                return false;
            }
            $this->is_login = true;
            return true;
        } catch(\Exception $e) {
            return false;
        }
    }

    private function createSearchUrl($keyword, $isAdult) {
        $base_url = ($isAdult) ? static::$search_adult : static::$search_normal;
        $url = sprintf($base_url,urlencode($keyword));
        return $url;
    }

    public function search($keyword, $isAdult = false) {
        $url = $this->createSearchUrl($keyword,$isAdult);
        return $this->getPageAll($url);
    }

    public function getPageAll($url) {
        $client = $this->client;
        $limit = $this->date_limit;
        $crawler = $client->request("GET",$url);
        $urls = array();
        try {
            while(true) {
                $hrefs = $crawler->filter(".image-item")->each(function($node) use ($client,$limit) {
                    $href = $node->filter("a")->attr("href");
                    $crawler = $client->request("GET","http://www.pixiv.net{$href}");
                    if($limit <= Date::dataToTime($crawler->filter("ul.meta > li:first-child")->text()) ) {
                        return "http://www.pixiv.net{$href}";
                    } else {
                        throw new \Exception("指定された日時に到達しました。");
                    }
                });
                if(empty($hrefs)) {
                    throw new \Exception("これ以上要素が取得できませんでした。");
                }
                $urls = array_merge($urls,array_filter($hrefs));
                if($next = $this->isNext($crawler)) {
                    $crawler = $client->request("GET","http://www.pixiv.net/search.php{$next}");
                } else {
                    throw new \Exception("ページ終端に到着しました。");
                }
            }
        } catch(\Exception $e) {
            $this->logger->info($e->getMessage());
        } catch(\InvalidArgumentException $e){
            $this->logger->warn("要素が取得できませんでした。");
        } finally {
            return new \ArrayObject($urls);
        }
    }

    public function getImages($urls,$save_dir) {
        $image = 0;
        if(!is_dir($save_dir)) {
            if(!mkdir($save_dir) && !is_writable($save_dir)) {
                throw new \Exception("ファイルの書き込み権限がありません。");
            }
        }
        $this->logger->info(count($urls)."件ダウンロードします。");
        foreach($urls as $url) {
            $crawler = $this->client->request("GET",$url);
            try {
                if(0 < count($crawler->filter(".multiple"))) {
                    $href = $crawler->filter(".works_display > a")->attr("href");
                    $crawler = $this->client->request("GET","http://www.pixiv.net/{$href}");
                    $crawler->filter(".item-container")->each(function($node) use ($save_dir,$image) {
                        $src = $node->filter("img")->attr("data-src");
                        $this->client->request("GET",$src);
                        $response = $this->client->getResponse();
                        $content = $response->getContent();
                        $content->seek(0);
                        $file = $content->getContents();
                        if(!file_put_contents($save_dir.basename($src),$file)) {
                            $this->logger->info("{$src}は保存できませんでした。");
                        }
                        $image++;
                        $this->logger->info("[multiple] ".$save_dir.basename($src));
                    });
                } else {
                    $src = $crawler->filter(".original-image")->attr("data-src");
                    $this->client->request("GET",$src);
                    $response = $this->client->getResponse();
                    $content = $response->getContent();
                    $content->seek(0);
                    $file = $content->getContents();
                    if(!file_put_contents($save_dir.basename($src),$file)) {
                        $this->logger->info("{$src}は保存できませんでした。");
                    }
                    $image++;
                    $this->logger->info("[single] ".$save_dir.basename($src));
                }
            } catch(\InvalidArgumentException $e) {
                $this->logger->warn("要素が取得できませんでした。");
            } catch(\GuzzleHttp\Exception\AdapterException $e) {
                $this->logger->warn("タイムアウトしました。");
            }
        }
        return $image;
    }

    private function isNext($crawler) {
        try {
            return $crawler->filter("a._button[rel='next']")->attr("href");
        } catch(\Exception $e) {
            return false;
        }
    }
}
