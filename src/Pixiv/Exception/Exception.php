<?php
namespace Pixiv\Exception;
class Exception extends \Exception
{
    protected static $severities = [
        E_ERROR             => 'ERROR',
        E_WARNING           => 'WARNING',
        E_PARSE             => 'PARSING ERROR',
        E_NOTICE            => 'NOTICE',
        E_CORE_ERROR        => 'CORE ERROR',
        E_CORE_WARNING      => 'CORE WARNING',
        E_COMPILE_ERROR     => 'COMPILE ERROR',
        E_COMPILE_WARNING   => 'COMPILE WARNING',
        E_USER_ERROR        => 'USER ERROR',
        E_USER_WARNING      => 'USER WARNING',
        E_USER_NOTICE       => 'USER NOTICE',
        E_STRICT            => 'STRICT NOTICE',
        E_RECOVERABLE_ERROR => 'RECOVERABLE ERROR',
        E_DEPRECATED        => 'DEPRECATED',
        E_USER_DEPRECATED   => 'USER DEPRECATED',
    ];

    /**
     * return full exception message
     * @return string
     */
    public function getFullMessage()
    {
        return sprintf('%s: %s in %s on line %u', $this->getSeverityString(), $this->getMessage(), $this->getFile(), $this->getLine());
    }

    /**
     * return the exception severity as string
     * @return string
     */
    public function getSeverityString()
    {
        return $this->_severityToString($this->getSeverity());
    }

    /**
     * return severity as string
     * @param int $severity
     * @return string
     */
    protected function _severityToString($severity)
    {
        if ( array_key_exists($severity, static::$severities) === true )
        {
            return static::$severities[$severity];
        }

        return 'UNKNOWN ERROR('.$severity.')';
    }
}
