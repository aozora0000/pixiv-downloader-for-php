<?php
namespace Pixiv\Exception;
class Handler {
    public static function setHandler() {
        set_error_handler(function($severity, $message, $file, $line) {
            throw new Exception($message);
        });
    }
}
