<?php
namespace Pixiv\Core;
class Provider
{
    protected $client;

    static $login_url = "https://www.secure.pixiv.net/login.php";
    static $my_page   = "http://www.pixiv.net/mypage.php#_=_";
    static $search_normal = "http://www.pixiv.net/search.php?s_mode=s_tag&word=%s";
    static $search_adult  = "http://www.pixiv.net/search.php?s_mode=s_tag&r18=1&word=%s";
}
