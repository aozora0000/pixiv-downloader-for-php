<?php
namespace Pixiv\Interfaces;
interface ProviderInterface
{
    /**
     * Pixiv Login Method
     * @param string $login_id
     * @param string $login_pass
     * @return boolean isLogin
     */
    public function login($login_id, $login_pass);

    /**
     * Pixiv SearchMethod
     * @param string $query
     * @param boolean $isAdult
     */
    public function search($query, $isAdult = false);

    /**
     * Pixiv SearchPage getAll
     * @param string $url
     * @return array
     */
    public function getPageAll($url);

    /**
     * Pixiv getImage
     * @param array $urls
     * @param string $save_dir
     */
    public function getImages($urls, $save_dir);
}
