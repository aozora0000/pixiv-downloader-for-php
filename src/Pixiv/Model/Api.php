<?php
namespace Pixiv\Model;
use \Pixiv\Exception\Exception;
use \ArrayObject;
class Api {
    public static function getIterator($url) {
        $file = file_get_contents($url);
        if(!$file) {
            throw new Exception("内容がありません。");
        }
        $arrayobject = new ArrayObject(json_decode($file));
        return $arrayobject->getIterator();
    }
}
