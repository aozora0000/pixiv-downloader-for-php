<?php
namespace Pixiv\Model;
use \Pixiv\Exception\Exception;
class Date {
    public static function dataToTime($str) {
        $needle_ym = array("年","月");
        $needle_d  = array("日");
        $str = str_replace($needle_ym,"-",$str);
        $str = str_replace($needle_d,"",$str);
        $str .= ":00";
        $str = date("Y-m-d H:i:s",strtotime($str));
        if(!preg_match("/^\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}$/",$str) || $str === "1970-01-01 09:00:00") {
            throw new Exception("Valid Failed! {$str}");
        }
        return $str;
    }
}
