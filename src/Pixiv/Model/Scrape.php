<?php
namespace Pixiv\Model;
class Scrape
{
    protected $queries;
    protected $container;
    public function __construct(\Pimple\Container $container) {
        $this->container = $container;
    }

    public function execute() {
        $container = $this->container;
        foreach($container['word'] as $query) {
            $client = $container['provider'];
            if($client->login(getenv("LOGIN_USER"),getenv("LOGIN_PASS"))) {
                $container['logger']->info("{$query->query}の検索開始");
                $singles = $client->search($query->query,true);
                if($singles) {
                    $count = $client->getImages($singles,$container['save_path']."/{$query->slug}/");
                    $container['logger']->info("{$count}件保存しました。");
                } else {
                    $container['logger']->info("画像が存在しませんでした。");
                }
            } else {
                $container['logger']->err("ログインできませんでした。");
                exit("ログインできませんでした。");
            }
        }
    }
}
